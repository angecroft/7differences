﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The sound effect manager is unique (singleton) and can play any sound.
/// </summary>
public class SoundEffectManager : MonoBehaviour 
{
	public static SoundEffectManager Instance;

	public AudioClip clicButtonSound;
	public AudioClip goodAnswerSound;
	public AudioClip badAnswerSound;
	public AudioClip winGameSound;
	public AudioClip loseGameSound;

	void Start () { Instance = this; }

	public void MakeClicButtonSound() { MakeSound(clicButtonSound); }	
	public void MakeGoodAnswerSound() { MakeSound(goodAnswerSound); }	
	public void MakeBadAnswerSound() { MakeSound(badAnswerSound); }
	public void MakeWinGameSound() { MakeSound(winGameSound); }
	public void MakeLoseGameSound() { MakeSound(loseGameSound); }
	
	/// <summary>
	/// Play a sound.
	/// </summary>
	/// <param name="originalClip"></param>
	private void MakeSound(AudioClip originalClip)
	{
		AudioSource.PlayClipAtPoint(originalClip, transform.position);
	}
}
