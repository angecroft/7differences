﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Translates the camera from one state to an other.
/// </summary>
public class CameraTranslate : MonoBehaviour 
{
	public const float smooth = 2.2f;

	private Vector3 _positionEnd;
	private bool _transitionFinished = true;

	public Vector3 GetPositionEnd() { return _positionEnd; }
	public void SetPositionEnd(Vector3 newPosition) { _positionEnd = newPosition; }
	public bool IsTransitionFinished() { return _transitionFinished; } 
	public void SetTransitionFinished(bool newTransitionFinished) { _transitionFinished = newTransitionFinished; }

	/// <summary>
	/// Changes the position on every frame with smoothly transition.
	/// </summary>
	public void PositionChanging()
	{
		if(Mathf.Abs(_positionEnd.x - transform.position.x) > 0.01f || 
		   Mathf.Abs(_positionEnd.y - transform.position.y) > 0.01f)
		{
			_transitionFinished = false;
			transform.position = Vector3.Lerp(transform.position, _positionEnd, Time.deltaTime * smooth);
		} else
		{
			transform.position = _positionEnd;
			_transitionFinished = true;
		}
	}

	void Start() { _positionEnd = transform.position; }

	void Update() { PositionChanging(); }
}
