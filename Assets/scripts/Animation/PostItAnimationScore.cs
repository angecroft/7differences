﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Post it animation  before the display of the score.
/// </summary>
public class PostItAnimationScore : MonoBehaviour 
{
	private Animator postItAnimator;
	private CameraTranslate mainCamera;

	/// <summary>
	/// Resets the animation of the post it.
	/// </summary>
	public void ResetAnimationPostIt()
	{
		postItAnimator.SetBool("transitionFinished", false);
		GetComponent<SpriteRenderer>().enabled = true;
		postItAnimator.enabled = true;
		postItAnimator.Play("postItStop");
	}

	void Start () 
	{
		postItAnimator = GetComponent<Animator>();
		mainCamera = Camera.main.GetComponent<CameraTranslate>();
	}

	void Update () 
	{
		if(StateManager.States.SCORE == StateManager.GetCurrentState() && mainCamera.IsTransitionFinished())
			postItAnimator.SetBool("transitionFinished", true);
	}
}
