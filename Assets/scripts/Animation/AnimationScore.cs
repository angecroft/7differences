﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Animation of the points earned (100 points).
/// </summary>
public class AnimationScore : MonoBehaviour 
{
	public Animator pointsPrefab;

	/// <summary>
	/// Creates the animation of the 100 points at the position where the player clicked.
	/// </summary>
	/// <param name="position">Position of the animation</param>
	public void CreateAnimation(Vector3 position)
	{
		position.z = -0.8f;
		Animator pointsAnim = Instantiate (pointsPrefab) as Animator;
		pointsAnim.transform.position = position;
		pointsAnim.Play("100");
	}
}
