﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Post it animation before the display of the leader board.
/// </summary>
public class PostItAnimationLeaderBoard : MonoBehaviour 
{
	public float timeElapsed {get; private set;}
	private Animator postItAnimator;
	private CameraTranslate mainCamera;
	private Animator postItScore;
	private bool enableAnimationLater;
	private int timeToWait = 10;

	/// <summary>
	/// Resets the animation of the post it.
	/// </summary>
	public void ResetAnimationPostIt()
	{
		postItAnimator.SetBool("animScoreFinished", false);
		GetComponent<SpriteRenderer>().enabled = true;
		postItAnimator.enabled = true;
		postItAnimator.Play("postItStop");
		timeElapsed = timeToWait;
		enableAnimationLater = false;
	}

	void Start () 
	{
		postItAnimator = GetComponent<Animator>();
		mainCamera = Camera.main.GetComponent<CameraTranslate>();
		postItScore = GameObject.Find("Post-it_score").GetComponent<Animator>();
		enableAnimationLater = false;
		timeElapsed = timeToWait;
	}

	void Update () 
	{
		if(StateManager.States.SCORE == StateManager.GetCurrentState() && 
		   true == mainCamera.IsTransitionFinished() &&
		   false == postItScore.enabled)
		{
			if(false == enableAnimationLater)
			{
				enableAnimationLater = true;
			} else
			{
				if(timeElapsed < 0)
					postItAnimator.SetBool("animScoreFinished", true);
			}
		}
	}

	void FixedUpdate()
	{
		if(StateManager.States.SCORE == StateManager.GetCurrentState() && 
		   true == mainCamera.IsTransitionFinished() &&
		   false == postItScore.enabled)
		{
			if(enableAnimationLater)
			{
				if(timeElapsed > 0) timeElapsed -= Time.deltaTime;
			}
		}
	}
}
