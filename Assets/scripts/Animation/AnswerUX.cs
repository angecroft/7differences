﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Displays an UX interaction when the player touch the image on the right.
/// </summary>
public class AnswerUX : MonoBehaviour 
{
	public GameObject cursorRed;
	public GameObject cursorGreen;

	private GameObject[] cursorGreensOfTheGame;
	private int index;

	/// <summary>
	/// Instantiate a circle prefab with the specified position.
	/// If the circle instantiated is a circle that will last all the time of the game, it must be saved 
	/// in an array in order to delete it easily later.
	/// </summary>
	/// <param name="position">Position of the GameObject</param>
	public void Instantiate(Vector3 position, bool correctAnswer)
	{
		Vector3 pos = position;
		pos.z = -0.7f;
		if(correctAnswer)
		{
			cursorGreensOfTheGame[index] = Instantiate (cursorGreen) as GameObject;
			cursorGreensOfTheGame[index].transform.position = pos;
			++index;
			SoundEffectManager.Instance.MakeGoodAnswerSound();
		} else
		{
			GameObject circle = Instantiate (cursorRed) as GameObject;
			circle.transform.position = pos;
			SoundEffectManager.Instance.MakeBadAnswerSound();
		}		
	}

	/// <summary>
	/// Reset the cursor array and delete all circle prefab that was instantiated.
	/// </summary>
	public void ResetCursors()
	{
		for(int i=0; i<index; ++i)
		{
			Destroy(cursorGreensOfTheGame[i]);
			cursorGreensOfTheGame[i] = null;
		}
		index = 0;
	}

	void Start()
	{
		cursorGreensOfTheGame = new GameObject[7];
		index = 0;
	}
}
