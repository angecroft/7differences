﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The Chrono class controls both the time update and its animation.
/// </summary>
public class Chrono: MonoBehaviour{
	public GameObject chrono;
	public float time;
	public bool isActive = true;

	private static Chrono instance;
	private Animator animChrono;
	private int startChrono = 120;
	private CameraTranslate mainCamera; 

	/// <summary>
	/// Gets the instance of the chrono or create it if it's not.
	/// </summary>
	public static Chrono Instance()
	{
		if(instance == null) 
			instance = GameObject.FindObjectOfType(typeof(Chrono)) as Chrono;

		return instance;
	}
	
	/// <summary>
	/// Gets the number of the start chrono.
	/// </summary>
	/// <returns>The start chrono.</returns>
	public int GetStartChrono()	{ return startChrono; }

	/// <summary>
	/// Stops the chrono and its animation.
	/// </summary>
	public void StopChrono()
	{
		isActive = false;
		animChrono.enabled = false;
	}

	/// <summary>
	/// Resets the chrono (controler of the time) and its animation.
	/// </summary>
	public void ResetChrono()
	{
		time = startChrono;
		animChrono.enabled = true;
		animChrono.Play("testStop");
		animChrono.SetBool("topChrono", false);
		isActive = true;
	}

	/// <summary>
	/// Constructor unauthorized
	/// </summary>
	private Chrono(){}

	void Start () 
	{
		Chrono.Instance();
		time = startChrono;
		animChrono = chrono.GetComponent<Animator>();
		mainCamera = Camera.main.GetComponent<CameraTranslate>();
	}

	void FixedUpdate () 
	{
		if(StateManager.States.GAME == StateManager.GetCurrentState() && mainCamera.IsTransitionFinished())
		{
			if(!isActive){
				animChrono.enabled = false;
			} else
			{
				if (time >= 0) 
				{
					animChrono.SetBool("topChrono", true);
					animChrono.enabled = true;
					time -= Time.deltaTime;
				} else if(time < -0.01f)
				{
					GameObject.Find ("Player").GetComponent<Health> ().enabled = false;
					GameObject.Find ("Player").GetComponent<Score> ().ComputeFinalscore();
					StopChrono();
					SoundEffectManager.Instance.MakeLoseGameSound();
					StartCoroutine (GameObject.Find ("Scripts").GetComponent<GeneralGame>().End ());
				}
			}
		}
	}
}
