﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The Cleaner class is called when the state changed to CONTEXT.
/// The Cleaner reset the game.
/// </summary>
public class Cleaner : MonoBehaviour 
{
	private AnswerUX cursor;
	private Chrono chrono;
	private Health health;
	private GameObject[] cursorGreens;
	private GeneralGame aleatoryImage;
	private GUIGame guiGame;
	private GUITutorial guiTutorial;
	private GUIPseudo guiPseudo;
	private PostItAnimationLeaderBoard postItLeaderBoard;
	private PostItAnimationScore postItScore;
	private Score score;
	private Tutorial tutorial;
	private GameObject scotoma;
	
	void Start () 
	{
		GameObject playerObject = GameObject.Find("Player");
		GameObject scriptsObject = GameObject.Find("Scripts");
		GameObject guiObject =  GameObject.Find("GUI/HUD");
		GameObject postItObject = GameObject.Find("Post-it_score");
		GameObject postItObject2 = GameObject.Find("Post-it_leaderboard");

		score = playerObject.GetComponent<Score>();
		chrono = GameObject.Find("ChronoScript").GetComponent<Chrono>();
		health = playerObject.GetComponent<Health>();
		cursor = scriptsObject.GetComponent<AnswerUX>();
		aleatoryImage = scriptsObject.GetComponent<GeneralGame>();
		tutorial = scriptsObject.GetComponent<Tutorial>();
		guiGame = guiObject.GetComponent<GUIGame>();
		guiTutorial = guiObject.GetComponent<GUITutorial>();
		guiPseudo = guiObject.GetComponent<GUIPseudo>();
		postItScore = postItObject.GetComponent<PostItAnimationScore>();
		postItLeaderBoard = postItObject2.GetComponent<PostItAnimationLeaderBoard>();
		scotoma = GameObject.Find("Scotoma");
	}

	/// <summary>
	/// Calls all function that reset any part of the game.
	/// </summary>
	public void CleanAll()
	{
		CleanAnimationHeart();
		score.ResetScore();
		chrono.ResetChrono();
		health.ResetHealth();
		cursor.ResetCursors();
		aleatoryImage.ResetImage();
		postItScore.ResetAnimationPostIt();
		postItLeaderBoard.ResetAnimationPostIt();
		guiPseudo.ResetGUIPseudo();
		guiGame.ResetScreenEndGame();
		scotoma.transform.position = new Vector3(-12.66f, 1.43f, -0.8f);
	}

	/// <summary>
	/// Cleans the animation heart.
	/// </summary>
	/// <returns>The animation heart.</returns>
	public void CleanAnimationHeart() { guiGame.DisableAnimation(); }

	/// <summary>
	/// Cleans the tutorial.
	/// </summary>
	public void CleanTuto()
	{ 
		aleatoryImage.LoadRandomImage(); 
		tutorial.ResetTutorial();
		cursor.ResetCursors();
		guiTutorial.ResetTimeElapsed();
	}
}

