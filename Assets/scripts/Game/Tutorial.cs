﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handles any animations and interaction of the state TUTORIAL.
/// </summary>
public class Tutorial : MonoBehaviour 
{
	public GameObject[] tintGameObjects;
	public GameObject[] imgTuto;

	public bool tutorialFinished {get; private set;}
	public float timeElapsedToColor {get; private set;}
	public float timeToColorImg {get; private set;}

	private float timeToWait, timeElapsed;
	private float deepColor;
	private bool circleInstance;
	private CameraTranslate mainCamera; 
	private GameObject scotoma;
	private MoveScotoma scotomaMoveScript;
	private AnswerUX circleUX ;
	private bool findAllObjects;

	/// <summary>
	/// Resets the tutorial.
	/// </summary>
	public void ResetTutorial()
	{
		RecolorObjects();
		circleInstance = false;
		tutorialFinished = false;
		timeToWait = 1.9f;
		timeToColorImg = 1.9f;
		timeElapsed = timeToWait;
		timeElapsedToColor = timeToColorImg;
		scotomaMoveScript.ResetAnimationTutoFinished();
		findAllObjects = false;
	}

	void Start () 
	{
		mainCamera = Camera.main.GetComponent<CameraTranslate>();
		scotoma = GameObject.Find("Scotoma");
		scotomaMoveScript = scotoma.GetComponent<MoveScotoma>();
		circleUX = gameObject.GetComponent<AnswerUX>();
		circleInstance = false;
		tutorialFinished = false;
		deepColor = 0.25f;
		timeToWait = 1.9f;
		timeToColorImg = 1.9f;
		timeElapsed = timeToWait;
		timeElapsedToColor = timeToColorImg;
		findAllObjects = false;
	}

	void Update()
	{
		if(StateManager.States.TUTORIAL == StateManager.GetCurrentState() && mainCamera.IsTransitionFinished())
		{
			if(circleInstance && !tutorialFinished)
			{
				// le joueur a cliqué et lancé l'animation du cercle
				if (Input.GetMouseButtonDown(0)) 
				{
					RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), 
					                                     Vector2.zero);
					Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					pz.z = 0;
					if (hit.collider != null && hit.collider.tag.Equals("img_tint"))
					{
						circleUX.Instantiate(pz, true);
						tutorialFinished = true;
					}
				}
			}
			
			// le scotome a fini de bouger
			if(circleInstance == false && scotomaMoveScript.TestScotoma())
			{
				// recoloration de la vignette de droite
				timeElapsedToColor -= Time.deltaTime;
				// creation de l'animation de clique
				if(timeElapsedToColor < 0)
				{
					circleInstance = true;
					circleUX.Instantiate(new Vector3(-4.53f, 1.55f, -0.8f), true);
				}
			}
		}
	}

	void FixedUpdate()
	{		
		if(StateManager.States.TUTORIAL == StateManager.GetCurrentState())
		{
			// fondu au noir des gameObjects
			if(timeElapsed > 0)
				timeElapsed -= Time.deltaTime;

			FindObjectWithTagTint();
			DiscolorObjects(timeElapsed, timeElapsedToColor);
		}
	}

	/// <summary>
	/// Discolors the objects.
	/// </summary>
	/// <param name="timeElapsed">Time elapsed.</param>
	/// <param name="timeElapsedToColor">Time elapsed to color.</param>
	private void DiscolorObjects(float timeElapsed, float timeElapsedToColor)
	{
		int speed = 3;
		for(int i=0; i<tintGameObjects.Length; ++i)
			tintGameObjects[i].renderer.material.color = new Color(deepColor+timeElapsed/speed, 
			                                                       deepColor+timeElapsed/speed,
			                                                       deepColor+timeElapsed/speed);

		if(Mathf.Abs(timeElapsedToColor - timeToColorImg) < 0.001)
			for(int i=0; i<imgTuto.Length; ++i)
				imgTuto[i].renderer.material.color = new Color(deepColor+timeElapsed/speed, 
				                                               deepColor+timeElapsed/speed, deepColor+timeElapsed/speed);
		else
			for(int i=0; i<imgTuto.Length; ++i)
				imgTuto[i].renderer.material.color = new Color(1-timeElapsedToColor/speed, 1-timeElapsedToColor/speed, 
				                                               1-timeElapsedToColor/speed);
	}

	/// <summary>
	/// Finds the object with tag tint.
	/// </summary>
	private void FindObjectWithTagTint()
	{
		if(!findAllObjects)
		{
			tintGameObjects = GameObject.FindGameObjectsWithTag("tint");
			imgTuto = GameObject.FindGameObjectsWithTag("img_tint");
			findAllObjects = true;
		}
	}

	
	/// <summary>
	/// Recolors the objects on the right. 
	/// </summary>
	private void RecolorObjects()
	{
		for(int i=0; i<tintGameObjects.Length; ++i)
			tintGameObjects[i].renderer.material.color = new Color(1, 1, 1);
	}
}
