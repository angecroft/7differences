﻿using UnityEngine;
using System.Collections;
/// <summary>
/// This class handles the aleatory load of the image when the game state begin.
/// Its also load the tutorial image when we enter in the tutorial state.
/// Be careful on Android Devices some GameObjects were deleted after a while (thanks to the GC). 
/// This is why they are created and destroyed when we load a new game.
/// </summary>
public class GeneralGame : MonoBehaviour 
{
	public GameObject currentImage;
	public GameObject[] images;
	public GameObject gum;
	public GameObject postItBase;
	public GameObject coffee;
	public GameObject pencilHome;
	public GameObject tabletBase;

	private GameObject gumInstance;
	private GameObject postItInstance;
	private GameObject coffeeInstance;
	private GameObject pencilHomeInstance;
	private GameObject tabletBaseInstance;
	private int nbOfDifferenceFound;
	private string nameOfPreviousImage;

	/// <summary>
	/// End of game.
	/// </summary>
	public IEnumerator  End()
	{
		yield return new WaitForSeconds(3.9f);
		StateManager.SetCurrentState(StateManager.States.SCORE);
	}
	
	/// <summary>
	/// Resets the scene. Reload the tutorial image.
	/// </summary>
	public void ResetImage()
	{
		Destroy(currentImage);
		Destroy(gumInstance);
		Destroy(postItInstance);
		Destroy(coffeeInstance);
		Destroy(pencilHomeInstance);
		Destroy(tabletBaseInstance);
		LoadTutoImage();
		gumInstance = Instantiate(gum) as GameObject;
		postItInstance = Instantiate(postItBase) as GameObject;
		coffeeInstance = Instantiate(coffee) as GameObject;
		pencilHomeInstance = Instantiate(pencilHome) as GameObject;
		tabletBaseInstance = Instantiate(tabletBase) as GameObject;
		nbOfDifferenceFound = 0;
	}

	public int GetNbOfDifferenceFound() { return nbOfDifferenceFound; }
	public void AddOneDifferenceFound() { ++nbOfDifferenceFound; }

	/// <summary>
	/// Loads a random image to play with.
	/// </summary>
	public void LoadRandomImage()
	{
		Destroy(currentImage);
		currentImage = GameObject.Instantiate(images[Random.Range(1, images.Length)]) as GameObject;
		if(nameOfPreviousImage.Equals(""))
		{
			nameOfPreviousImage = currentImage.name;
		} else if(!nameOfPreviousImage.Equals(""))
		{
			if(nameOfPreviousImage.Equals(currentImage.name))
			{
				while(nameOfPreviousImage.Equals(currentImage.name))
				{
					Destroy(currentImage);
					currentImage = GameObject.Instantiate(images[Random.Range(1, images.Length)]) as GameObject;
				}
			}
			nameOfPreviousImage = currentImage.name;
		}
	}

	void LoadTutoImage() { currentImage = GameObject.Instantiate(images[0]) as GameObject; }
	
	void Start () 
	{
		LoadTutoImage ();
		gumInstance = Instantiate(gum) as GameObject;
		postItInstance = Instantiate(postItBase) as GameObject;
		coffeeInstance = Instantiate(coffee) as GameObject;
		pencilHomeInstance = Instantiate(pencilHome) as GameObject;
		tabletBaseInstance = Instantiate(tabletBase) as GameObject;
		nbOfDifferenceFound = 0;
//		Screen.showCursor = false;
		nameOfPreviousImage = "";
	}
}
