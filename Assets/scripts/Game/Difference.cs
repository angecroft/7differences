﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Figures it out if the player has touched a difference or if its an error.
/// This class calls anything that depends on it like the score, the life, the animations.
/// </summary>
public class Difference : MonoBehaviour 
{
	public bool gameComplete {get; private set;}

	private AnswerUX cursorCircle;
	private Collider2D[] areas;			// zones des différences
	private AnimationScore animationScore;
	private Score scorePlayer;
	private CameraTranslate mainCamera;
	private GeneralGame nbOfDifference;
	
	void Start () 
	{
		GameObject scriptsObject = GameObject.Find ("Scripts");
		areas = this.GetComponentsInChildren<Collider2D>();
		animationScore = scriptsObject.GetComponent<AnimationScore>();
		cursorCircle = scriptsObject.GetComponent<AnswerUX>();
		nbOfDifference = scriptsObject.GetComponent<GeneralGame>();
		scorePlayer = GameObject.Find ("Player").GetComponent<Score>();
		mainCamera = Camera.main.GetComponent<CameraTranslate>();
	}

	void Update () 
	{
		if(StateManager.States.GAME == StateManager.GetCurrentState() &&
		   mainCamera.IsTransitionFinished() && !gameComplete && 0 != Health.lifes)
		{
			if (Input.GetMouseButtonDown(0)) 
			{
				RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
				// on passe du repere de la souris a celui du monde unity
				Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				pz.z = 0;
				/////////////////////
				// DIFFERENCE FOUND
				if (hit.collider != null && hit.collider.tag.Equals("target"))
				{
					hit.collider.enabled = false;
					nbOfDifference.AddOneDifferenceFound();			
					gameComplete = FoundAllDifferences ();
					scorePlayer.UpdateScore();
					cursorCircle.Instantiate(pz, true);
					animationScore.CreateAnimation (pz);
					if(gameComplete)
					{
						Chrono.Instance().StopChrono();
						scorePlayer.ComputeFinalscore();
						GameObject.Find ("Player").GetComponent<Health> ().enabled = false;
						SoundEffectManager.Instance.MakeWinGameSound();
						StartCoroutine (GameObject.Find ("Scripts").GetComponent<GeneralGame>().End ());
					}
				} else if (hit.collider != null && hit.collider.tag.Equals("clickable") && Chrono.Instance().isActive)
				{
					cursorCircle.Instantiate(pz, false);
				}
			}
		}
	}

	/// <summary>
	/// Finds out if the player has found all the differences.
	/// </summary>
	/// <returns><c>true</c>, if all differences was founded, <c>false</c> otherwise.</returns>
	bool FoundAllDifferences()
	{
		foreach (Collider2D area in areas) 
		{
			if(area.tag.Equals("target") && area.enabled == true) return false;
		}
		return true;
	}
}
