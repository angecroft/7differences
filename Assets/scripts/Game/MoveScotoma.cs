﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handles the movementss of the scotoma.
/// </summary>
public class MoveScotoma : MonoBehaviour 
{	
	private CameraTranslate mainCamera;
	private bool animationTutoFinished = false;

	/// <summary>
	/// Tests if the tutorial for the scotoma is finished.
	/// </summary>
	/// <returns><c>true</c>, if this part of the tutorial is finished, <c>false</c> otherwise.</returns>
	public bool TestScotoma()
	{
		if(!animationTutoFinished)
		{
			if(AnimateForTutorial())
				animationTutoFinished = true;
			return false;
		} else
		{
			if(TestMovementByUser())
				return true;
			else
				return false;
		}
	}

	/// <summary>
	/// Resets the animation tuto finished.
	/// </summary>
	public void ResetAnimationTutoFinished() { animationTutoFinished = false; }

	void Start() { mainCamera = Camera.main.GetComponent<CameraTranslate>(); }

	void Update () 
	{
		if(StateManager.States.GAME == StateManager.GetCurrentState() && mainCamera.IsTransitionFinished() &&
		   Chrono.Instance().isActive)
		{
			if(true == Input.GetMouseButton(0))
			{
				Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				position.z = 0;
				RaycastHit2D hit = Physics2D.Raycast(position, Vector2.zero);

				if (hit.collider != null && hit.collider.tag.Equals("scotoma") && 	// le scotome bouge uniquement sur la zone de l'image 2
				    Mathf.Abs(position.x - transform.position.x) < 1 && 			// on peut bouger que si on est sur le scotome et
				    Mathf.Abs(position.y - transform.position.y) < 1)				// pas a coté
				{
					position.z = -0.8f;
					transform.position = position;
				}		
			}
		}
	}

	/// <summary>
	/// Moves the scotoma with smooth for the tutorial state.
	/// </summary>
	/// <returns><c>true</c>, if the scotoma move is finished, <c>false</c> otherwise.</returns>
	private bool AnimateForTutorial()
	{
		Vector3 positionEnd = new Vector3(-11.8f, 3.17f, -0.8f);
		if(Mathf.Abs(positionEnd.x - transform.position.x) > 0.01 || 
		   Mathf.Abs(positionEnd.y - transform.position.y) > 0.01)
		{
			transform.position = Vector3.Lerp(transform.position, positionEnd, Time.deltaTime * 1.5f);
			return false;
		} else
		{
			transform.position = positionEnd;
			return true;
		}
	}
	
	/// <summary>
	/// Tests the movement by user.
	/// </summary>
	/// <returns><c>true</c>, if movement by user was tested, <c>false</c> otherwise.</returns>
	private bool TestMovementByUser()
	{
		Vector3 positionBegin = new Vector3(-11.8f, 3.17f, -0.8f);
		
		if(true == Input.GetMouseButton(0))
		{
			Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			position.z = 0;
			RaycastHit2D hit = Physics2D.Raycast(position, Vector2.zero);
			
			if (hit.collider != null && hit.collider.tag.Equals("scotoma") && 	// le scotome bouge uniquement sur la zone de l'image 2
			    Mathf.Abs(position.x - transform.position.x) < 1 && 			// on peut bouger que si on est sur le scotome et
			    Mathf.Abs(position.y - transform.position.y) < 1)				// pas a coté
			{
				position.z = -0.8f;
				transform.position = position;
			}		
			return false;
		} else
		{
			if(!transform.position.x.Equals(positionBegin.x) && !transform.position.y.Equals(positionBegin.y))
				return true;
			else
				return false;
		}
	}
}
