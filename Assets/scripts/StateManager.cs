﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The state manager is unique (singleton) and its controls the different states of the game.
/// </summary>
public static class StateManager
{
	public enum States
	{
		CONTEXT,
		PSEUDO,
		TUTORIAL,
		GAME,
		SCORE,
		EXPLANATION
	};

	private static States currentState = StateManager.States.CONTEXT;
	private static Camera mainCamera = Camera.main;
	private static CameraTranslate scriptCamera = mainCamera.GetComponent<CameraTranslate>();
	private static Cleaner cleanEverything =  mainCamera.GetComponent<Cleaner>();

	public static void SetCurrentState(States newState)
	{
		currentState = newState;
		OnEnterState(currentState);
	}

	public static States GetCurrentState() { return currentState; }

	/// <summary>
	/// When incoming in a different state, the camera has a translation.
	/// In terms of which state is entering, the camera will tranlate with a diffrent Vector3.
	/// </summary>
	/// <param name="stateEnter">The State incoming.</param>
	private static void OnEnterState(States stateEnter)
	{
		scriptCamera.SetTransitionFinished(false);
		if(StateManager.States.CONTEXT == stateEnter) 
		{
			cleanEverything.CleanAll();
			scriptCamera.SetPositionEnd(new Vector3(-8.28f, -7.54f, -10.0f));
		} else if(StateManager.States.PSEUDO == stateEnter)
		{
			scriptCamera.SetPositionEnd(new Vector3(3.6f, -7.54f, -10.0f));
		} else if(StateManager.States.TUTORIAL == stateEnter)
		{
			scriptCamera.SetPositionEnd(new Vector3(-8.28f, 3.0f, -10.0f));
		} else if(StateManager.States.GAME == stateEnter)
		{
			cleanEverything.CleanTuto();
		} else if(StateManager.States.SCORE == stateEnter)
		{
			Chrono.Instance().StopChrono();
			scriptCamera.SetPositionEnd(new Vector3(-1.27f, 8.5f, -10.0f));
		} else if(StateManager.States.EXPLANATION == stateEnter)
		{
			scriptCamera.SetPositionEnd(new Vector3(9.45f, 8.5f, -10.0f));
		}
	}
}
