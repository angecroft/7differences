﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Sets the GUI of state TUTORIAL.
/// </summary>
public class GUITutorial : MonoBehaviour 
{
	public GUIStyle chronoTuto;
	public GUIStyle tutorial;
	public GUIStyle departure;

	private GameObject[] lifes;
	private Tools tools;
	private float timeRemaining;
	private CameraTranslate mainCamera; 
	private Tutorial tutorialScript;
	private const float timeToWait = 6;
	private float timeElapsed;

	/// <summary>
	/// Resets the life animation.
	/// </summary>
	public void ResetLifeAnimation()
	{
		for(int i=0; i<Health.lifesStart; ++i)
		{
			Animator animator = lifes[i].GetComponent<Animator>();
			animator.SetBool("loseALife", false);
			animator.enabled = false;
			animator.enabled = true;
			animator.Play("HeartbeatAnim");
		}
	}

	/// <summary>
	/// Resets the time elapsed.
	/// </summary>
	public void ResetTimeElapsed() { timeElapsed = timeToWait; }

	void Start () 
	{
		tools = GetComponent<Tools>();
		timeRemaining = Chrono.Instance().time;
		mainCamera = Camera.main.GetComponent<CameraTranslate>();
		tutorialScript = GameObject.Find("Scripts").GetComponent<Tutorial>();
		lifes = GetComponent<GUIGame>().lifes;
		timeElapsed = timeToWait;
	}

	void OnGUI()
	{
		int buttonWidth = 200;
		int buttonHeight = 100;
		Vector2 pointWithGUICoords = Vector2.zero;

		if(StateManager.States.TUTORIAL == StateManager.GetCurrentState())
		{
			/////////////////////
			// CHRONO
			int timeInSecondes = Mathf.CeilToInt (timeRemaining);
			int minutes = timeInSecondes / 60;
			int seconds = timeInSecondes % 60;
			string text = String.Format ("{0:0}:{1:00}", minutes, seconds); 
			
			pointWithGUICoords = tools.WorldToGUI(new Vector2(-8.30f, 6.45f));
			GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
			                    buttonWidth, buttonHeight), text, chronoTuto );

			if (mainCamera.IsTransitionFinished())
			{
				/////////////////////
				// LEFT IMAGE
				pointWithGUICoords = tools.WorldToGUI(new Vector2(-11.8f, -0.46f));	
				GUIUtility.RotateAroundPivot(-1.5f, pointWithGUICoords);
				GUI.Label( new Rect(pointWithGUICoords.x - 350/2, pointWithGUICoords.y - buttonHeight/2,
				                    350, buttonHeight),
				          "Maintiens ton doigt appuyé pour déplacer la tache grise.", tutorial );
				GUIUtility.RotateAroundPivot(1.5f, pointWithGUICoords);

				/////////////////////
				// RIGHT IMAGE
				if(Mathf.Abs(tutorialScript.timeElapsedToColor - tutorialScript.timeToColorImg) > 0.001)
				{
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-4.9f, -0.5f));		
					GUIUtility.RotateAroundPivot(5, pointWithGUICoords);
					GUI.Label( new Rect(pointWithGUICoords.x - 350/2, pointWithGUICoords.y - buttonHeight/2,
					                    350, buttonHeight),
					          "Clique sur cette vignette pour sélectionner une différence.", tutorial );
					GUIUtility.RotateAroundPivot(-5, pointWithGUICoords);
				}
				if(tutorialScript.tutorialFinished)
				{
					buttonWidth = 200;
					buttonHeight = 120;
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-8.28f, 3));	
					if(timeElapsed < 0)
					{
						ResetLifeAnimation();
						StateManager.SetCurrentState(StateManager.States.GAME);
					} else if(timeElapsed < 1.1 && timeElapsed > 0.3)
					{
						GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
						                     buttonWidth, buttonHeight), "1", departure );
					} else if(timeElapsed < 2.1 && timeElapsed > 1.3)
					{
						GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
						                    buttonWidth, buttonHeight), "2", departure );
					} else if(timeElapsed < 3.1 && timeElapsed > 2.3)
					{
						GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
						                    buttonWidth, buttonHeight), "3", departure );
					} else if (timeElapsed < timeToWait && timeElapsed > 3.3)
					{
						GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
						                    buttonWidth, buttonHeight), "PRÊT ?", departure );
					}
				}
			}
		}
	}

	void FixedUpdate()
	{
		if(tutorialScript.tutorialFinished)
			timeElapsed -= Time.deltaTime;
	}
}
