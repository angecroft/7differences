﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Sets the GUI of the state CONTEXT.
/// </summary>
public class GUIContext : MonoBehaviour 
{
	public GUIStyle button1; 
	public GUIStyle button2; 
	public GUIStyle button3; 

	private Tools tools;

	void Start() { tools = GetComponent<Tools>(); }

	void OnGUI()
	{
		if(StateManager.States.CONTEXT == StateManager.GetCurrentState())
		{
			const int buttonWidth = 340;
			const int buttonHeight = 159;
			Vector2 pointWithGUICoords = Vector2.zero;

			pointWithGUICoords = tools.WorldToGUI(new Vector2(-6.3f, -6.0f));
			if( GUI.Button( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
			                         buttonWidth, buttonHeight), "Nouvelle partie", button3))
			{
				SoundEffectManager.Instance.MakeClicButtonSound();
				StateManager.SetCurrentState(StateManager.States.PSEUDO);
			}
		}
	}
}
