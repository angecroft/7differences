﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Sets the GUI of the state GAME.
/// </summary>
public class GUIGame : MonoBehaviour 
{
	public GUIStyle chrono; 
	public GUIStyle styleLifes;
	public GUIStyle scoreStateGame;
	public GUIStyle pseudoStateGame;
	public GUIStyle ruleNumber;
	public GUIStyle timesUp;

	public GameObject[] lifes;

	private SpriteRenderer endGameRenderer;
	private float timeRemaining;
	private CameraTranslate mainCamera; 
	private GeneralGame generalGame;
	private Score score;
	private Tools tools;
	private const float timeToWait = 8;
	private float timeElapsed;

	/// <summary>
	/// Change the state of animation of a life object to loseALife.
	/// </summary>
	public void LoseALifeAnimation()
	{
		lifes[Health.lifes].GetComponent<Animator>().SetBool("loseALife", true);
	}

	/// <summary>
	/// Disables the animation of the hearts.
	/// </summary>
	public void DisableAnimation()
	{
		for(int i=0; i<Health.lifesStart; ++i)
		{
			Animator animator = lifes[i].GetComponent<Animator>();
			animator.Play("HeartbeatDeathAnim");
		}
	}

	/// <summary>
	/// Resets the screen end game.
	/// </summary>
	public void ResetScreenEndGame() { timeElapsed = 0; }

	void Start()
	{
		timeRemaining = Chrono.Instance().time;
		mainCamera = Camera.main.GetComponent<CameraTranslate>();
		generalGame = GameObject.Find("Scripts").GetComponent<GeneralGame>();
		score = GameObject.Find("Player").GetComponent<Score>();
		tools = GetComponent<Tools>();
		DisableAnimation();
		endGameRenderer = GameObject.Find("End_game").GetComponent<SpriteRenderer>();
		timeElapsed = 0;
	}

	void FixedUpdate()
	{
		if(7 == generalGame.GetNbOfDifferenceFound() || 0 == Health.lifes || 0 > timeRemaining)
		{
			if(timeElapsed < timeToWait)
				timeElapsed += Time.deltaTime;
		}			
	}

	void Update() { timeRemaining = Chrono.Instance().time; }

	void OnGUI()
	{
		int buttonWidth = 200;
		int buttonHeight = 100;
		Vector2 pointWithGUICoords = Vector2.zero;

		/////////////////////
		// SCORE NON FINAL
		if(StateManager.States.GAME == StateManager.GetCurrentState() ||	
		   (StateManager.States.SCORE == StateManager.GetCurrentState() && mainCamera.IsTransitionFinished() == false))
		{
			pointWithGUICoords = tools.WorldToGUI(new Vector2(-2.9f, 6.3f));				
			GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
			                    buttonWidth, buttonHeight), Score.points.ToString(), scoreStateGame );

			pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.5f, 7));				
			GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2, 
			                    buttonWidth, buttonHeight), score.GetPseudoPlayer(), pseudoStateGame );
		}

		if(StateManager.States.GAME == StateManager.GetCurrentState() ||		
		   StateManager.States.SCORE == StateManager.GetCurrentState())
		{				
			/////////////////////
			// CHRONO
			int timeInSecondes = Mathf.CeilToInt (timeRemaining);
			int minutes = timeInSecondes / 60;
			int seconds = timeInSecondes % 60;
			string text = String.Format ("{0:0}:{1:00}", minutes, seconds); 

			pointWithGUICoords = tools.WorldToGUI(new Vector2(-8.30f, 6.45f));
			GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
			                    buttonWidth, buttonHeight), text, chrono );

			/////////////////////
			// NUMBER OF DIFFERENCE
			const float rotAngle = 356;
			Vector2 pivotPoint = Vector2.zero;

			for(int i=7; i>0; --i)
			{
				pivotPoint = new Vector2(-16.31f + 0.58f*i, 6.7f);
				pointWithGUICoords = tools.WorldToGUI(pivotPoint);
				GUIUtility.RotateAroundPivot(rotAngle, pivotPoint);
				if(i <= generalGame.GetNbOfDifferenceFound())
				{
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), i.ToString(), ruleNumber );
				} else
				{
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), i.ToString(), styleLifes );
				}
				GUIUtility.RotateAroundPivot(-rotAngle, pivotPoint);
			}

			/////////////////////
			// END OF GAME 

			/////////////////////
			// ALL DIFFERENCES FOUND
			if(7 == generalGame.GetNbOfDifferenceFound() && timeElapsed < timeToWait)
			{
				FadeInAndOutEndGame();
				pointWithGUICoords = tools.WorldToGUI(new Vector2(-8.28f, 3));
				GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
				                    buttonWidth, buttonHeight), "Gagné !", timesUp );
			}

			/////////////////////
			// NO LIFES
			if(0 == Health.lifes && timeElapsed < timeToWait)
			{
				FadeInAndOutEndGame();
				pointWithGUICoords = tools.WorldToGUI(new Vector2(-8.28f, 3));
				GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
				                    buttonWidth, buttonHeight), "Perdu !", timesUp );
			}

			/////////////////////
			// TIME'S UP
			if(0 > timeRemaining && timeElapsed < timeToWait)
			{
				FadeInAndOutEndGame();
				pointWithGUICoords = tools.WorldToGUI(new Vector2(-8.28f, 3));
				GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
				                    buttonWidth, buttonHeight), "Temps écoulé !", timesUp );
			}
		}
	}

	/// <summary>
	/// Fades the in and out end game screen.
	/// </summary>
	private void FadeInAndOutEndGame()
	{
		if(timeElapsed >= timeToWait*0.35f)
		{
			endGameRenderer.color = new Color(1, 1, 1, 2-(timeElapsed*0.48f));
			GUI.color = new Color(1, 1, 1, 2-timeElapsed*0.48f);
		} else if(timeElapsed > 0)
		{
			endGameRenderer.color = new Color(1, 1, 1, (timeElapsed/4)*3.0f);
			GUI.color = new Color(1, 1, 1, (timeElapsed/4)*3.0f);
		}
	}
}
