using UnityEngine;
using System.Collections;

/// <summary>
/// Sets the GUI of state SCORE.
/// </summary>
public class GUIScore : MonoBehaviour 
{
	public GUIStyle scoreBold;
	public GUIStyle scorePointsStateScore;
	public GUIStyle buttonAccueil;
	public GUIStyle totalBar;

	private CameraTranslate mainCamera; 
	private PostItAnimationLeaderBoard postItLeaderBoard;
	private Tools tools;

	void Start () 
	{
		mainCamera = Camera.main.GetComponent<CameraTranslate>();
		postItLeaderBoard = GameObject.Find("Post-it_leaderboard").GetComponent<PostItAnimationLeaderBoard>();
		tools = GetComponent<Tools>();
	}

	void OnGUI()
	{
		const int buttonWidth = 200;
		const int buttonHeight = 100;
		Vector2 pointWithGUICoords = Vector2.zero;

		/////////////////
		// SCORE FINAL
		if(StateManager.States.SCORE == StateManager.GetCurrentState() && mainCamera.IsTransitionFinished())
		{
			if(!GameObject.Find("Post-it_score").GetComponent<Animator>().enabled && 0 < postItLeaderBoard.timeElapsed)							
			{
				/////////////////
				// POINTS
				pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 10.1f));				
				GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
				                    buttonWidth, buttonHeight), "Points :", scoreBold );
				
				pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 9.65f));				
				GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
				                    buttonWidth, buttonHeight), Score.points.ToString(), scorePointsStateScore );
				
				/////////////////
				// BONUS
				pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 9.05f));				
				GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
				                    buttonWidth, buttonHeight), "Bonus temps :", scoreBold );
				
				pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 8.6f));				
				GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
				                    buttonWidth, buttonHeight), Score.bonusPoints.ToString(), scorePointsStateScore );
				
				/////////////////
				// MALUS + SCORE FINAL
				if(Health.lifes != Health.lifesStart && Score.points > 0)
				{
					/////////////////
					// MALUS
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 8.15f));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), "Malus vies perdues :", scoreBold );
					
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 7.7f));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), "-"+Score.malusPoints.ToString(), scorePointsStateScore );
					
					/////////////////
					// SCORE TOTAL
					scorePointsStateScore.normal.textColor = new Color(0, 0.31f, 0.35f, 1);
					scoreBold.normal.textColor = new Color(0, 0.31f, 0.35f, 1);
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 7.5f));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), "_______", scorePointsStateScore );

					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 7.1f));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), "Score final :", scoreBold );
					
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 6.65f));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), Score.finalScore.ToString(), scoreBold );
					scoreBold.normal.textColor = new Color(0, 0, 0, 1);
					scorePointsStateScore.normal.textColor = new Color(0, 0, 0, 1);
				} else 		// Score total si aucun malus 
				{
					scorePointsStateScore.normal.textColor = new Color(0, 0.31f, 0.35f, 1);
					scoreBold.normal.textColor = new Color(0, 0.31f, 0.35f, 1);
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 8.3f));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), "_______", scorePointsStateScore );

					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 7.7f));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), "Score final :", scoreBold );
					
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.27f, 7.25f));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), Score.finalScore.ToString(), scoreBold );
					scoreBold.normal.textColor = new Color(0, 0, 0, 1);
					scorePointsStateScore.normal.textColor = new Color(0, 0, 0, 1);
				}
			}
		}

		if(StateManager.States.EXPLANATION == StateManager.GetCurrentState())
		{
			pointWithGUICoords = tools.WorldToGUI(new Vector2(3.6f, 11.9f));
			if( GUI.Button( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
			                         buttonWidth, buttonHeight), "Accueil", buttonAccueil ))
			{
				SoundEffectManager.Instance.MakeClicButtonSound();
				StateManager.SetCurrentState(StateManager.States.CONTEXT);
			}
		}
	}
}
