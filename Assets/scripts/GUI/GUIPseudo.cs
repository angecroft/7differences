﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Sets the GUI of the state PSEUDO.
/// </summary>
public class GUIPseudo : MonoBehaviour 
{
	public GUIStyle stylePencil;
	public GUIStyle stylePseudo;
	public Texture2D[] texNbNormal;
	public Texture2D[] texNbActive;
	public Texture2D[] texLine1Normal;
	public Texture2D[] texLine1Active;
	public Texture2D[] texLine2Normal;
	public Texture2D[] texLine2Active;
	public Texture2D[] texLine3Normal;
	public Texture2D[] texLine3Active;
	public GameObject backgroundKeyBoard;
 
	private GUIStyle keystroke;
	private string pseudo;
	private CameraTranslate mainCamera; 
	private GameObject background;
	private const int lengthMaxPseudo = 8;
	private Tools tools;

	/// <summary>
	/// Resets the pseudo of the GUI.
	/// </summary>
	public void ResetGUIPseudo() { pseudo = ""; }

	void Start () 
	{ 
		keystroke = new GUIStyle(); 
		pseudo = "";
		mainCamera = Camera.main.GetComponent<CameraTranslate>();
		background = null;
		tools = GetComponent<Tools>();
	}

	void OnGUI()
	{
		if(StateManager.States.PSEUDO == StateManager.GetCurrentState())
		{
			Vector2 pointWithGUICoords = Vector2.zero;
			///////////////////////
			/// LABEL PENCIL
			pointWithGUICoords = tools.WorldToGUI(new Vector2(2, -3.9f));				
			GUI.Label( new Rect(pointWithGUICoords.x - 80/2, pointWithGUICoords.y - 20/2,
			                    80, 20), "Entrez un pseudonyme :", stylePencil );

			///////////////////////
			/// LABEL PSEUDO
			pointWithGUICoords = tools.WorldToGUI(new Vector2(9.5f, -4.0f));		
			string pseudoDisplay = UpdatePseudoGUI(pseudo);
			GUI.Label( new Rect(pointWithGUICoords.x - 80/2, pointWithGUICoords.y - 20/2,
			                    80, 20), pseudoDisplay, stylePseudo );

			///////////////////////
			/// KEYBOARD
			if(mainCamera.IsTransitionFinished()) 	
			{
				int buttonWidthNumber = (int)(texNbActive[0].width * 0.5f);
				int buttonHeightNumber = (int)(texNbActive[0].height * 0.5f);

				if(Screen.width > 1800 && Screen.height > 900)
				{
					buttonWidthNumber = (int)(texNbActive[0].width * 0.8f);
					buttonHeightNumber = (int)(texNbActive[0].height * 0.8f);
				}

				float pos = Screen.width * 0.9f;
				float offset = Screen.width * 0.05f;
				
				///////////////////////
				/// BACKGROUND
				if (null == background) background = Instantiate(backgroundKeyBoard) as GameObject;
				
				///////////////////////
				/// NUMBER KEYS
				for(int i=0; i<texNbNormal.Length; ++i)
				{
					keystroke.normal.background = texNbNormal[i];
					keystroke.active.background = texNbActive[i];
					keystroke.onActive.background = texNbActive[i];

					pointWithGUICoords = tools.WorldToGUI(new Vector2(-4, -8.2f));
					
					if( GUI.Button( new Rect((i*pos / texNbNormal.Length) + offset,
					                         pointWithGUICoords.y - buttonHeightNumber/2,
					                         buttonWidthNumber, buttonHeightNumber), "", keystroke))
					{
						SoundEffectManager.Instance.MakeClicButtonSound();
						if(lengthMaxPseudo > pseudo.Length) pseudo += texNbNormal[i].name;
					}
				}
				
				int buttonWidthLine = (int)(texLine1Active[0].width * 0.5f);
				int buttonHeightLine = (int)(texLine1Active[0].height * 0.5f);

				if(Screen.width > 1800 && Screen.height > 900)
				{
					buttonWidthLine = (int)(texLine1Active[0].width * 0.8f);
					buttonHeightLine = (int)(texLine1Active[0].height * 0.8f);
				}

				///////////////////////
				/// LINE ONE KEYS
				for(int i=0; i<texLine1Active.Length; ++i)
				{
					keystroke.normal.background = texLine1Normal[i];
					keystroke.active.background = texLine1Active[i];
					keystroke.onActive.background = texLine1Active[i];
					
					///////////////////////
					/// BACK KEY
					if(i == texLine1Active.Length-1)
					{
						int buttonBackWidth = (int)(texLine1Active[i].width * 0.5f);
						int buttonBackHeight = (int)(texLine1Active[i].height * 0.5f);

						if(Screen.width > 1800 && Screen.height > 800)
						{
							buttonBackWidth = (int)(texLine1Active[i].width * 0.8f);
							buttonBackHeight = (int)(texLine1Active[i].height * 0.8f);
						}
						
						if( GUI.Button( new Rect((i*pos / (texLine1Active.Length+1)) + offset,
						                         (pointWithGUICoords.y + buttonHeightNumber + 10) - buttonBackHeight/2,
						                         buttonBackWidth, buttonBackHeight), "", keystroke))
						{
							SoundEffectManager.Instance.MakeClicButtonSound();
							if(0 < pseudo.Length) pseudo = pseudo.Substring(0, pseudo.Length-1);
						}
					} else
					{				
						if( GUI.Button( new Rect((i*pos / (texLine1Active.Length+1)) + offset,
						                         (pointWithGUICoords.y + buttonHeightNumber + 10) - buttonHeightLine/2,
						                         buttonWidthLine, buttonHeightLine), "", keystroke))
						{
							SoundEffectManager.Instance.MakeClicButtonSound();
							if(lengthMaxPseudo > pseudo.Length) pseudo += texLine1Normal[i].name;
						}
					}
				}
				
				///////////////////////
				/// LINE TWO KEYS
				for(int i=0; i<texLine2Active.Length; ++i)
				{
					keystroke.normal.background = texLine2Normal[i];
					keystroke.active.background = texLine2Active[i];
					keystroke.onActive.background = texLine2Active[i];
					
					///////////////////////
					/// ENTER KEY
					if(i == texLine1Active.Length-1)
					{
						int buttonBackWidth = (int)(texLine2Active[i].width * 0.5f);
						int buttonBackHeight = (int)(texLine2Active[i].height * 0.5f);

						if(Screen.width > 1800 && Screen.height > 800)
						{
							buttonBackWidth = (int)(texLine2Active[i].width * 0.8f);
							buttonBackHeight = (int)(texLine2Active[i].height * 0.8f);
						}
						
						if( GUI.Button( new Rect((i*pos / (texLine2Active.Length+1)) + offset,
						                         (pointWithGUICoords.y + buttonHeightNumber + buttonHeightLine + 10)
						                         - buttonBackHeight/2, buttonBackWidth, buttonBackHeight), 
						               "", keystroke))
						{
							SoundEffectManager.Instance.MakeClicButtonSound();
							if(0 < pseudo.Length)
							{
								int nbPlayer = PlayerPrefs.GetInt("nbPlayer");
								PlayerPrefs.SetInt("nbPlayer", ++nbPlayer);
								GameObject.Find("Player").GetComponent<Score>().SetPseudoPlayer(pseudo);
								StateManager.SetCurrentState(StateManager.States.TUTORIAL);
							}
						}
					} else
					{				
						if( GUI.Button( new Rect((i*pos / (texLine1Active.Length+1)) + offset,
						                         (pointWithGUICoords.y + buttonHeightNumber + buttonHeightLine + 10) 
						                         - buttonHeightLine/2, buttonWidthLine, buttonHeightLine), 
						               "", keystroke))
						{
							SoundEffectManager.Instance.MakeClicButtonSound();
							if(lengthMaxPseudo > pseudo.Length) pseudo += texLine2Normal[i].name;
						}
					}
				}
				
				///////////////////////
				/// LINE THREE KEYS
				float pos2 = Screen.width * 0.6f;
				float offset2 = Screen.width * 0.1f;
				for(int i=0; i<texLine3Active.Length; ++i)
				{
					keystroke.normal.background = texLine3Normal[i];
					keystroke.active.background = texLine3Active[i];
					keystroke.onActive.background = texLine3Active[i];
					
					///////////////////////
					/// SPACE KEY
					if(i == texLine3Active.Length-1)
					{
						int buttonBackWidth = (int)(texLine3Active[i].width * 0.5f);
						int buttonBackHeight = (int)(texLine3Active[i].height * 0.5f);

						if(Screen.width > 1800 && Screen.height > 800)
						{
							buttonBackWidth = (int)(texLine3Active[i].width * 0.8f);
							buttonBackHeight = (int)(texLine3Active[i].height * 0.8f);
						}
						
						if( GUI.Button( new Rect((i*pos2 / (texLine3Active.Length+1)) + offset2,
						                         (pointWithGUICoords.y + buttonHeightNumber + 2*buttonHeightLine + 10)
						                         - buttonBackHeight/2, buttonBackWidth, buttonBackHeight), 
						               "", keystroke))
						{
							SoundEffectManager.Instance.MakeClicButtonSound();
							if(lengthMaxPseudo > pseudo.Length) pseudo += " ";
						}
					} else
					{				
						if( GUI.Button( new Rect((i*pos2 / (texLine3Active.Length+1)) + offset2,
						                         (pointWithGUICoords.y + buttonHeightNumber + 2*buttonHeightLine + 10) 
						                         - buttonHeightLine/2, buttonWidthLine, buttonHeightLine), 
						               "", keystroke))
						{
							SoundEffectManager.Instance.MakeClicButtonSound();
							if(lengthMaxPseudo > pseudo.Length) pseudo += texLine3Normal[i].name;
						}
					}
				}
			}
		} else
		{
			if (null != background) Destroy(background);
		}
	}

	/// <summary>
	/// Updates the pseudo GUI.
	/// </summary>
	/// <returns>The pseudo of the player.</returns>
	/// <param name="pseudo">Pseudo.</param>
	private string UpdatePseudoGUI(string pseudo)
	{
		if(lengthMaxPseudo != pseudo.Length)
		{
			string temp = pseudo;
			while(temp.Length < lengthMaxPseudo)
			{
				temp += "-";
			}
			return temp;
		}
		return pseudo;
	}
}
