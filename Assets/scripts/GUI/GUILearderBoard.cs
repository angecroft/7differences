﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Sets the GUI of the learder board (on state SCORE).
/// </summary>
public class GUILearderBoard : MonoBehaviour 
{
	public GUIStyle style;
	public GUIStyle styleBold;
	public GUIStyle buttonAccueil;
	public GUIStyle buttonExplanation;

	private Animator postItScore;
	private CameraTranslate mainCamera; 
	private Tools tools;
	private Score scorePlayer;
	private int nbPlayerDisplayed = 3;
		
	void Start () 
	{
		mainCamera = Camera.main.GetComponent<CameraTranslate>();
		postItScore = GameObject.Find("Post-it_leaderboard").GetComponent<Animator>();
		scorePlayer = GameObject.Find("Player").GetComponent<Score>();
		tools = GetComponent<Tools>();
	}

	void OnGUI()
	{
		const int buttonWidth = 200;
		const int buttonHeight = 100;
		Vector2 pointWithGUICoords = Vector2.zero;

		if(StateManager.States.SCORE == StateManager.GetCurrentState() && mainCamera.IsTransitionFinished() && 
		   !postItScore.enabled)
		{	
			pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.7f, 9.8f ));	
			GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
			                    buttonWidth, buttonHeight), "Classement :", styleBold );

			/////////////////
			// TOP 3
			for(int i=1; i<=nbPlayerDisplayed; ++i)
			{
				pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.7f, 9.8f - (0.5f*i)));	
				if(i == scorePlayer.rankPlayer)
				{
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), i+". "+ PlayerPrefs.GetString("name"+i)+" : "+
					          PlayerPrefs.GetInt("score"+i), styleBold );
				} else
				{
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight), i+". "+ PlayerPrefs.GetString("name"+i)+" : "+
					          PlayerPrefs.GetInt("score"+i), style );
				}				
			}
				
			/////////////////
			// RANK PLAYER IF ITS NOT <= nbPlayerDisplayed
			if(nbPlayerDisplayed < scorePlayer.rankPlayer)
			{
				if(scorePlayer.rankPlayer == nbPlayerDisplayed + 1)
				{					
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.7f, 9.8f - (0.5f*(nbPlayerDisplayed+1))));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight),
					          scorePlayer.rankPlayer+". "+ PlayerPrefs.GetString("name"+scorePlayer.rankPlayer)+
					          " : "+PlayerPrefs.GetInt("score"+scorePlayer.rankPlayer), styleBold );
				} else
				{
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.7f, 9.8f - (0.5f*(nbPlayerDisplayed+0.8f))));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight),
					          " . . . ", style );
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.7f, 9.8f - (0.5f*(nbPlayerDisplayed+1.6f))));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight),
					          " . . . ", style );
					
					pointWithGUICoords = tools.WorldToGUI(new Vector2(-1.7f, 9.8f - (0.5f*(nbPlayerDisplayed+2.6f))));				
					GUI.Label( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
					                    buttonWidth, buttonHeight),
					          scorePlayer.rankPlayer+". "+ PlayerPrefs.GetString("name"+scorePlayer.rankPlayer)+
					          " : "+PlayerPrefs.GetInt("score"+scorePlayer.rankPlayer), styleBold );
				}

			}

			/////////////////
			// BUTTONS
			pointWithGUICoords = tools.WorldToGUI(new Vector2(3.6f, 11.9f));
			if( GUI.Button( new Rect(pointWithGUICoords.x - buttonWidth/2, pointWithGUICoords.y - buttonHeight/2,
			                         buttonWidth, buttonHeight), "Accueil", buttonAccueil ))
			{
				StateManager.SetCurrentState(StateManager.States.CONTEXT);
			}
			
			const int newButtonWidth = 300;
			const int newButtonHeight = 160;
			pointWithGUICoords = tools.WorldToGUI(new Vector2(3.6f, 10.2f));
			if( GUI.Button( new Rect(pointWithGUICoords.x - newButtonWidth/2, pointWithGUICoords.y - newButtonHeight/2,
			                         newButtonWidth, newButtonHeight), "Qu'est-ce que la DMLA ?", buttonExplanation ))
			{
				SoundEffectManager.Instance.MakeClicButtonSound();
				StateManager.SetCurrentState(StateManager.States.EXPLANATION);
			}
		}
	}
}
