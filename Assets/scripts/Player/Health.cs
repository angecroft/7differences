﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Health of the player.
/// </summary>
public class Health : MonoBehaviour 
{
	public static int lifes {get; private set;}
	public static int lifesStart = 3;
	private CameraTranslate mainCamera; 
	private GUIGame GUIGameComponent;
	private AnswerUX cursorCircle;

	/// <summary>
	/// Resets the health count of the player.
	/// </summary>
	public void ResetHealth()
	{
		lifes = 3;
		this.enabled = true;
	}

	void Start () 
	{	
		lifes = 3;
		mainCamera = Camera.main.GetComponent<CameraTranslate>();
		GUIGameComponent = GameObject.Find("GUI/HUD").GetComponent<GUIGame>();
		cursorCircle = GameObject.Find ("Scripts").GetComponent<AnswerUX>();
	}

	void Update () 
	{
		if(StateManager.States.GAME == StateManager.GetCurrentState() && mainCamera.IsTransitionFinished())
		{
			if (Input.GetMouseButtonDown(0)) 
			{
				RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
				Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				if (hit.collider != null && hit.collider.tag.Equals("clickable"))
				{
					if(lifes !=0)
					{
						--lifes;
						GUIGameComponent.LoseALifeAnimation();
					} 
					if(lifes == 0 && Chrono.Instance().isActive)
					{
						cursorCircle.Instantiate(pz, false);
						GameObject.Find("Player").GetComponent<Score>().ComputeFinalscore();
						Chrono.Instance().StopChrono();
						SoundEffectManager.Instance.MakeLoseGameSound();
						StartCoroutine (GameObject.Find ("Scripts").GetComponent<GeneralGame>().End ());
					}						
				}			
			}
		}
	}
}
