﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Calculates the score of a game play.
/// </summary>
public class Score : MonoBehaviour
{
	public static int points {get; private set;}
	public static int finalScore {get; private set;}
	public static int bonusPoints {get; private set;}
	public static int malusPoints {get; private set;}
	public int rankPlayer {get; private set;}
	private float timeBonus;
	private string pseudoPlayer;
	private UpdateLeaderBoard leaderboard;

	/// <summary>
	/// Updates the score in the game state.
	/// </summary>
	public void UpdateScore(){ points += 100; }

	public void SetPseudoPlayer(string pseudo) { pseudoPlayer = pseudo; }
	public string GetPseudoPlayer() { return pseudoPlayer; }
	
	/// <summary>
	/// Computes the finalscore.
	/// </summary>
	public void ComputeFinalscore()
	{
		if(GameObject.FindObjectOfType<Difference>().GetComponent<Difference>().gameComplete)
			timeBonus = Chrono.Instance().time;		
		ComputeBonusTime();
		ComputeHealthPoint ();
		finalScore = bonusPoints + points - malusPoints;
		if(finalScore < 0) finalScore = 0;
		rankPlayer = leaderboard.UpdateBoard(finalScore, pseudoPlayer);
	}

	/// <summary>
	/// Resets the score at the end of a game.
	/// </summary>
	public void ResetScore()
	{
		points = 0;
		bonusPoints = 0;
		timeBonus = 0;
		finalScore = 0;
		malusPoints = 0;
		pseudoPlayer = "";
	}

	void Start()
	{
		ResetScore();
		leaderboard = GameObject.Find("Scripts").GetComponent<UpdateLeaderBoard>();
	}

	/// <summary>
	/// Computes the bonus time.
	/// </summary>
	void ComputeBonusTime()
	{
		if (timeBonus <= 0)	bonusPoints = 0; 								// aucun point bonus

		if (timeBonus <= Chrono.Instance ().GetStartChrono ()* 0.20) 		// il reste moins de 20% du temps total
		{
			bonusPoints = (int) Math.Round (timeBonus*7) ;
		} else if (timeBonus <= Chrono.Instance ().GetStartChrono ()* 0.40) // il reste entre 20% et 40% du temps total
		{
			bonusPoints = (int) Math.Round (timeBonus*9) ;
		} else if (timeBonus <= Chrono.Instance ().GetStartChrono ()* 0.60) // il reste entre 40% et 60% du temps total
		{
			bonusPoints = (int) Math.Round (timeBonus*12) ;
		} else
		{
			bonusPoints = (int) Math.Round (timeBonus*15) ;
		}
	}

	/// <summary>
	/// Computes the health point.
	/// </summary>
	void ComputeHealthPoint()
	{
		// le joueur a perdu des points de vies
		if (Health.lifes != Health.lifesStart) malusPoints = ( Health.lifesStart - Health.lifes ) * 50;
	}
}
