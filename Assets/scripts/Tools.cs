﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Contains different tools. Functions like end of animation behavior.
/// </summary>
public class Tools : MonoBehaviour 
{
	/// <summary>
	/// Converts World coordinates to GUI coordinates with a vector2.
	/// </summary>
	/// <returns>The new point in GUI coordinates.</returns>
	/// <param name="position">Position.</param>
	public Vector2 WorldToGUI(Vector2 position)
	{
		Vector3 point = Camera.main.WorldToScreenPoint(new Vector3(position.x, position.y, 0));
		point.y = Screen.height - point.y;
		return point;
	}

	/// <summary>
	/// Destroies the game object.
	/// </summary>
	public void DestroyGameObject () { Destroy (gameObject); }

	/// <summary>
	/// Disables the animator of the object.
	/// </summary>
	public void DisableAnimator() { gameObject.GetComponent<Animator>().enabled = false; }

	/// <summary>
	/// Hides the post-it after its animation.
	/// </summary>
	public void HidePostIt()
	{
		DisableAnimator();
		if(!GetComponent<Animator>().enabled) GetComponent<SpriteRenderer>().enabled = false;
	}
}
