﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Exit the game on android devices with the back button.
/// </summary>
public class ExitGame : MonoBehaviour 
{
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) 
			Application.Quit(); 
	}
}
