﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Updates the leader board.
/// </summary>
public class UpdateLeaderBoard : MonoBehaviour 
{
	private int nbPlayer;

	/// <summary>
	/// Updates the leaderboard.
	/// </summary>
	/// <param name="scoreOfTheGame">Score of the actual game.</param>
	/// <param name="nameOfPlayer">Name of the actual player.</param>
	public int UpdateBoard(int scoreOfTheGame, string nameOfPlayer)
	{
		nbPlayer = PlayerPrefs.GetInt("nbPlayer");
		int rank = CompareLeaderboard(scoreOfTheGame);
		if(0 < rank)
		{
			for(int i=nbPlayer; i>=rank; --i)
			{
				int oldScore = PlayerPrefs.GetInt("score"+i);
				string oldName = PlayerPrefs.GetString("name"+i);
				PlayerPrefs.SetInt("score"+(i+1), oldScore);
				PlayerPrefs.SetString("name"+(i+1), oldName);
			}
			PlayerPrefs.SetInt("score"+rank, scoreOfTheGame);
			PlayerPrefs.SetString("name"+rank, nameOfPlayer);
			return rank;
		}
		else 	// nouveau joueur (dernier)
		{
			PlayerPrefs.SetInt("score"+nbPlayer, scoreOfTheGame);
			PlayerPrefs.SetString("name"+nbPlayer, nameOfPlayer);
			return nbPlayer;
		}
	}

	/// <summary>
	/// Compares the actual score of the player with the scores of the leaderboard.
	/// Returns the rank of the player if he/she enters in the leaderboard, 0 otherwise.
	/// </summary>
	/// <returns>The rank of the actual player.</returns>
	/// <param name="scoreOfTheGame">Score of the actual game.</param>
	private int CompareLeaderboard(int scoreOfTheGame)
	{
		for(int i=1; i<nbPlayer; ++i)
			if(PlayerPrefs.GetInt("score"+i) < scoreOfTheGame) return i;
		return 0;
	}
}
